console.log("Welcome, Arn Mendoza");

// Practice Javascript Coding Session Dec 12, 2022 Time Start: 3:47 pm Time Finished

// Topic: Comparison Operators

/*

	Operator           Name			       	   Example

	  ===		  Strict equality		  	  5 === 2 + 3		 

	  !==		Strict non-equality       	  5 !== 2 + 4 

	  < 			 Less than			   		6 < 10		

	  >			    Greater than				20 > 10

	 <=			 Less than or equal to  		5 <= 10

	 >=			Greater than or equal to  		10 >= 5 


*/